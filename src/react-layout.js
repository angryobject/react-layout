import React from 'react';
import reactHTML from 'reactHTML';

const blackListProps = ['reactHTML']
   .reduce((acc, v) => {
      acc[v] = true;
      return acc;
   }, {});

function stripProps(props) {
   return Object.keys(props)
      .filter(k => !blackListProps[k])
      .reduce((acc, k) => {
         acc[k] = props[k];
         return acc;
      }, {});
}

function layout(descriptor, components) {
   if (!descriptor) { return null; }

   return descriptor.map((v, i) =>
      React.createElement(
         components[v.type] || v.type,
         Object.assign({ key: i }, stripProps(v), v.reactHTML ? reactHTML(v.reactHTML) : null),
         (typeof v.children === 'string') ? v.children : layout(v.children, components)
      )
   );
}

const Layout = ({ className, descriptor = [], components = {} }) => (
   <div className={className}>
      {layout(descriptor, components)}
   </div>
);

Layout.propTypes = {
   className: React.PropTypes.string,
   descriptor: React.PropTypes.array,
   components: React.PropTypes.object,
};

export default Layout;
