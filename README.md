A react component to dynamically create layouts defined by a descriptor array.

Example:

```javascript
import React from 'react';
import ReactDOM from 'react-dom';
import Layout from 'react-layout';

const descriptor = [
   {
      type: 'div',
      children: [
         {
            type: 'h1',
            className: 'title',
            children: 'This is title'
         },
         {
            type: 'p',
            className: 'text',
            reactHTML: 'This is text containing <strong>html</strong> entities'
         }
      ]
   },
   {
      type: 'CustomComponent',
      message: 'This is custom component'
   }
];

// can also be a react class
const CustomComponent = (props) => (
   <div className="CustomComponent">
     {props.message}
   </div>
);

CustomComponent.propTypes = {
   message: React.PropTypes.string
};

ReactDOM.render(
  <Layout
     descriptor={descriptor}
     components={{
        CustomComponent
     }}
  />,
  document.getElementById('root')
);
```
